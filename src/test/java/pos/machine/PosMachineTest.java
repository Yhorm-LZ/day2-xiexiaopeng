package pos.machine;

import org.junit.jupiter.api.Test;

import java.util.*;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PosMachineTest {

    @Test
    public void should_return_receipt() {
        PosMachine posMachine = new PosMachine();

        String expected = "***<store earning no money>Receipt***\n" +
                "Name: Coca-Cola, Quantity: 4, Unit price: 3 (yuan), Subtotal: 12 (yuan)\n" +
                "Name: Sprite, Quantity: 2, Unit price: 3 (yuan), Subtotal: 6 (yuan)\n" +
                "Name: Battery, Quantity: 3, Unit price: 2 (yuan), Subtotal: 6 (yuan)\n" +
                "----------------------\n" +
                "Total: 24 (yuan)\n" +
                "**********************";

        assertEquals(expected, posMachine.printReceipt(loadBarcodes()));
    }

    @Test
    public void should_return_receiptContentMap(){
        ReceiptContentTransformer receiptContentTransformer=new ReceiptContentTransformer();
        List<String>barcodes= new ArrayList<>();
        barcodes.add("ITEM000000");
        barcodes.add("ITEM000000");
        List<Item> item=ItemsLoader.loadAllItems();
        HashMap<String, ReceiptContent> stringReceiptContentHashMap = receiptContentTransformer.buildReceiptContentMap(barcodes, item);

        HashMap<String,ReceiptContent>expected=new HashMap<>();
        ReceiptContent receiptContent=new ReceiptContent("Coca-Cola",2,3,6);
        expected.put("ITEM000000",receiptContent);

        assertEquals(expected.get("ITEM000000").toString(),stringReceiptContentHashMap.get("ITEM000000").toString());
    }

    @Test
    public void should_return_receiptBodyLine(){
        ReceiptContent receiptContent=new ReceiptContent("Coca-Cola",4,3,12);
        ReceiptBuilder receiptBuilder=new ReceiptBuilder();
        String receiptBodyLine = receiptBuilder.generateReceiptBodyLine(receiptContent);

        String expected="Name: Coca-Cola, Quantity: 4, Unit price: 3 (yuan), Subtotal: 12 (yuan)";

        assertEquals(expected,receiptBodyLine);
    }

    @Test
    public void should_return_receiptBody(){
        LinkedHashMap<String,ReceiptContent> receiptContentHashMap=new LinkedHashMap<>();
        ReceiptContent receiptContent1=new ReceiptContent("Coca-Cola",4,3,12);
        ReceiptContent receiptContent2=new ReceiptContent("Sprite",2,3,6);
        receiptContentHashMap.put("ITEM000000",receiptContent1);
        receiptContentHashMap.put("ITEM000001",receiptContent2);
        ReceiptBuilder receiptBuilder=new ReceiptBuilder();
        String receiptBody = receiptBuilder.generateReceiptBody(receiptContentHashMap);

        String expected="Name: Coca-Cola, Quantity: 4, Unit price: 3 (yuan), Subtotal: 12 (yuan)\n"+
                "Name: Sprite, Quantity: 2, Unit price: 3 (yuan), Subtotal: 6 (yuan)\n";

        assertEquals(expected,receiptBody);
    }


    private static List<String> loadBarcodes() {
        return Arrays.asList("ITEM000000", "ITEM000000", "ITEM000000", "ITEM000000", "ITEM000001", "ITEM000001", "ITEM000004", "ITEM000004", "ITEM000004");
    }
}
