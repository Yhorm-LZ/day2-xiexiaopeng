package pos.machine;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

public class ReceiptContentTransformer {
    public LinkedHashMap<String, ReceiptContent> receiptContentMap = new LinkedHashMap<>();

    public HashMap<String, ReceiptContent> buildReceiptContentMap(List<String> barcodes, List<Item> items) {
        for (String barcode : barcodes) {
            transferBarcodeToReceiptContent(barcode, items);
        }
        return receiptContentMap;
    }

    public void transferBarcodeToReceiptContent(String barcode, List<Item> items) {
        if (receiptContentMap.containsKey(barcode)) {
            ReceiptContent receiptContent = receiptContentMap.get(barcode);
            receiptContent.setQuantity(receiptContent.getQuantity() + 1);
            receiptContent.setSubtotal(receiptContent.getQuantity() * receiptContent.getUnitPrice());
        } else {
            for (Item item : items) {
                if (barcode.equals(item.getBarcode())) {
                    ReceiptContent receiptContent = new ReceiptContent(item.getName(), 1, item.getPrice(), item.getPrice());
                    receiptContentMap.put(barcode, receiptContent);
                }
            }
        }
    }
}
