package pos.machine;

import java.util.HashMap;
import java.util.List;

public class ReceiptBuilder {
    public String buildReceipt(List<String>barcodes,List<Item>items){
        ReceiptContentTransformer receiptContentTransformer=new ReceiptContentTransformer();
        HashMap<String, ReceiptContent> receiptContentHashMap = receiptContentTransformer.buildReceiptContentMap(barcodes,items);
        return "***<store earning no money>Receipt***\n" +
                generateReceiptBody(receiptContentHashMap) +
                "----------------------\n" +
                "Total: " +
                calculateTotal(receiptContentHashMap) +
                " (yuan)\n" +
                "**********************";
    }
    public String generateReceiptBody(HashMap<String,ReceiptContent>receiptContentHashMap){
        StringBuilder receiptBody=new StringBuilder();
        int index=0;
        for(ReceiptContent receiptContent:receiptContentHashMap.values()){
            receiptBody.append(generateReceiptBodyLine(receiptContent));
            receiptBody.append("\n");
        }
        return receiptBody.toString();
    }
    public int calculateTotal(HashMap<String,ReceiptContent>receiptContentHashMap){
        int total=0;
        for(ReceiptContent receiptContent:receiptContentHashMap.values()){
            total+=receiptContent.getSubtotal();
        }
        return total;
    }
    public String generateReceiptBodyLine(ReceiptContent receiptContent){
        return "Name: " +
                receiptContent.getName() +
                ", Quantity: " +
                receiptContent.getQuantity() +
                ", Unit price: " +
                receiptContent.getUnitPrice() +
                " (yuan), Subtotal: " +
                receiptContent.getSubtotal() +
                " (yuan)";
    }
}
