package pos.machine;

import java.util.HashMap;
import java.util.List;

public class PosMachine {
    public String printReceipt(List<String> barcodes) {
        List<Item> items=ItemsLoader.loadAllItems();
        ReceiptContentTransformer receiptContentTransformer=new ReceiptContentTransformer();
        HashMap<String,ReceiptContent>receiptContentHashMap=receiptContentTransformer.buildReceiptContentMap(barcodes,items);
        ReceiptBuilder receiptBuilder=new ReceiptBuilder();
        return receiptBuilder.buildReceipt(barcodes, items);
    }
}
