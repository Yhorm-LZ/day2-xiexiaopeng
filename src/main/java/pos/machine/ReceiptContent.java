package pos.machine;

public class ReceiptContent {
    private String name;
    private int quantity;
    private int unitPrice;
    private int Subtotal;

    public ReceiptContent(String name, int quantity, int unitPrice, int subtotal) {
        this.name = name;
        this.quantity = quantity;
        this.unitPrice = unitPrice;
        Subtotal = subtotal;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(int unitPrice) {
        this.unitPrice = unitPrice;
    }

    public int getSubtotal() {
        return Subtotal;
    }

    public void setSubtotal(int subtotal) {
        Subtotal = subtotal;
    }

}
